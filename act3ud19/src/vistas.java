import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

public class vistas extends JFrame{

	private final ButtonGroup buttonGroup = new ButtonGroup();
	public vistas() {
		
		getContentPane().addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		setBounds(100, 100, 415, 281);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Windows");
		buttonGroup.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("Linux");
		buttonGroup.add(rdbtnNewRadioButton_1);
		
		JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("Mac");
		buttonGroup.add(rdbtnNewRadioButton_2);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("Programaci\u00F3n");
		
		JCheckBox chckbxNewCheckBox_1 = new JCheckBox("Dise\u00F1o gr\u00E1fico");
		
		JCheckBox chckbxNewCheckBox_2 = new JCheckBox("Administraci\u00F3n");
		
		JSlider slider = new JSlider(0,10);
		
		JLabel lblNewLabel = new JLabel("Sistema operativo:");
		
		JLabel lblNewLabel_1 = new JLabel("Especialidad");
		
		JLabel lblNewLabel_2 = new JLabel("Horas dedicadas al pc");
		
		
		JButton btnNewButton = new JButton("Enviar");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ArrayList<String> marcat = new ArrayList<String>();
				if(rdbtnNewRadioButton.isSelected())
					marcat.add(rdbtnNewRadioButton.getText());
				if(rdbtnNewRadioButton_1.isSelected())
					marcat.add(rdbtnNewRadioButton_1.getText());
				if(rdbtnNewRadioButton_2.isSelected())
					marcat.add(rdbtnNewRadioButton_2.getText());
				
				if(chckbxNewCheckBox.isSelected())
					marcat.add(chckbxNewCheckBox.getText());
				if(chckbxNewCheckBox_1.isSelected())
					marcat.add(chckbxNewCheckBox_1.getText());
				if(chckbxNewCheckBox_2.isSelected())
					marcat.add(chckbxNewCheckBox_2.getText());
					
				Object[] marcats = marcat.toArray();
				
				int contador= 0; String resposta = "Heu introduit els valors: ";
				while(contador<=marcats.length-1) {
					
					resposta = resposta + "\n- "+ marcats[contador];
						contador++;
				}
				resposta = resposta + "\n- "+slider.getValue();
				
				JOptionPane.showMessageDialog(null, resposta);
				
			}
		});
		
		
		
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(36)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(btnNewButton)
								.addComponent(lblNewLabel_2)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(10)
									.addComponent(slider, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(24)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(rdbtnNewRadioButton)
										.addComponent(rdbtnNewRadioButton_1)
										.addComponent(rdbtnNewRadioButton_2)))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(32)
									.addComponent(lblNewLabel)))
							.addGap(18)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNewLabel_1)
								.addComponent(chckbxNewCheckBox_1)
								.addComponent(chckbxNewCheckBox)
								.addComponent(chckbxNewCheckBox_2))))
					.addContainerGap(153, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(22)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(lblNewLabel_1))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(rdbtnNewRadioButton)
						.addComponent(chckbxNewCheckBox))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(rdbtnNewRadioButton_1)
						.addComponent(chckbxNewCheckBox_1))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(rdbtnNewRadioButton_2)
						.addComponent(chckbxNewCheckBox_2))
					.addPreferredGap(ComponentPlacement.RELATED, 14, Short.MAX_VALUE)
					.addComponent(lblNewLabel_2)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(slider, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnNewButton)
					.addGap(24))
		);
		getContentPane().setLayout(groupLayout);
	}
}
