import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

public class vistas extends JFrame{
	
	public vistas() {

	setBounds(100, 100, 240, 220);
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
	JLabel fraseInicial = new JLabel("Escribe un nombre para saludar");
	
	JTextPane usuario = new JTextPane();
	
	JButton saludarBoton = new JButton("\u00A1saludar!");
	saludarBoton.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent arg0) {
			JOptionPane.showMessageDialog(null, "Et saludem " + usuario.getText() + "!");
		}
	});
	GroupLayout groupLayout = new GroupLayout(getContentPane());
	groupLayout.setHorizontalGroup(
		groupLayout.createParallelGroup(Alignment.LEADING)
			.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
				.addContainerGap(231, Short.MAX_VALUE)
				.addComponent(saludarBoton)
				.addGap(80))
			.addGroup(groupLayout.createSequentialGroup()
				.addGap(22)
				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
					.addComponent(fraseInicial)
					.addComponent(usuario, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE))
				.addContainerGap(17, Short.MAX_VALUE))
	);
	groupLayout.setVerticalGroup(
		groupLayout.createParallelGroup(Alignment.LEADING)
			.addGroup(groupLayout.createSequentialGroup()
				.addGap(22)
				.addComponent(fraseInicial)
				.addPreferredGap(ComponentPlacement.UNRELATED)
				.addComponent(usuario, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
				.addGap(28)
				.addComponent(saludarBoton)
				.addContainerGap(63, Short.MAX_VALUE))
	);
	getContentPane().setLayout(groupLayout);
	
	}
}
